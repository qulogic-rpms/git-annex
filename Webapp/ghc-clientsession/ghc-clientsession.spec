# generated by cabal-rpm-0.10.0
# https://fedoraproject.org/wiki/Packaging:Haskell

%global pkg_name clientsession

%bcond_with tests

Name:           ghc-%{pkg_name}
Version:        0.9.1.2
Release:        1%{?dist}
Summary:        Securely store session data in a client-side cookie

License:        MIT
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkg_name}-%{version}/%{pkg_name}-%{version}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  chrpath
BuildRequires:  ghc-base64-bytestring-devel
BuildRequires:  ghc-bytestring-devel
BuildRequires:  ghc-cereal-devel
BuildRequires:  ghc-cipher-aes-devel
BuildRequires:  ghc-cprng-aes-devel
BuildRequires:  ghc-crypto-api-devel
BuildRequires:  ghc-crypto-random-devel
BuildRequires:  ghc-directory-devel
BuildRequires:  ghc-entropy-devel
BuildRequires:  ghc-setenv-devel
BuildRequires:  ghc-skein-devel
BuildRequires:  ghc-tagged-devel
%if %{with tests}
BuildRequires:  ghc-HUnit-devel
BuildRequires:  ghc-QuickCheck-devel
BuildRequires:  ghc-containers-devel
BuildRequires:  ghc-hspec-devel
BuildRequires:  ghc-transformers-devel
%endif
# End cabal-rpm deps

%description
Achieves security through AES-CTR encryption and Skein-MAC-512-256
authentication. Uses Base64 encoding to avoid any issues with characters.


%package devel
Summary:        Haskell %{pkg_name} library development files
Provides:       %{name}-static = %{version}-%{release}
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development files.


%prep
%setup -q -n %{pkg_name}-%{version}


%build
%ghc_lib_build


%install
%ghc_lib_install

%ghc_fix_dynamic_rpath clientsession-generate


%check
%cabal_test


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%license LICENSE


%files devel -f %{name}-devel.files
%doc ChangeLog.md README.md
%{_bindir}/clientsession-generate


%changelog
* Fri Jul 21 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.9.1.1-3
- Bump for Fedora 26.

* Thu Dec 15 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.9.1.1-2
- Update release to be newer than previous builds

* Thu Dec 15 2016 Fedora Haskell SIG <haskell@lists.fedoraproject.org> - 0.9.1.1-1
- spec file generated by cabal-rpm-0.10.0
