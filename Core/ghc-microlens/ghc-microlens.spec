# generated by cabal-rpm-0.11
# https://fedoraproject.org/wiki/Packaging:Haskell

%global pkg_name microlens

Name:           ghc-%{pkg_name}
Version:        0.4.8.0
Release:        1%{?dist}
Summary:        A tiny lens library with no dependencies. If you're writing an app, you probably want microlens-platform, not this

License:        BSD
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkg_name}-%{version}/%{pkg_name}-%{version}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros

%description
This library is an extract from <http://hackage.haskell.org/package/lens lens>
(with no dependencies). It's not a toy lenses library, unsuitable for “real
world”, but merely a small one. It is compatible with lens, and should have
same performance. It also has better documentation.



%package devel
Summary:        Haskell %{pkg_name} library development files
Provides:       %{name}-static = %{version}-%{release}
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development files.


%prep
%setup -q -n %{pkg_name}-%{version}


%build
%ghc_lib_build


%install
%ghc_lib_install


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%license LICENSE


%files devel -f %{name}-devel.files
%doc CHANGELOG.md


%changelog
* Sat Jul 22 2017 Fedora Haskell SIG <haskell@lists.fedoraproject.org> - 0.4.8.0-1
- spec file generated by cabal-rpm-0.11
