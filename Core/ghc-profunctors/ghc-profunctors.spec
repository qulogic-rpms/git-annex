# generated by cabal-rpm-0.10.0
# https://fedoraproject.org/wiki/Packaging:Haskell

%global pkg_name profunctors

Name:           ghc-%{pkg_name}
Version:        5.2
Release:        4%{?dist}
Summary:        Profunctors

License:        BSD
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkg_name}-%{version}/%{pkg_name}-%{version}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  ghc-base-orphans-devel
BuildRequires:  ghc-bifunctors-devel
BuildRequires:  ghc-comonad-devel
BuildRequires:  ghc-contravariant-devel
BuildRequires:  ghc-distributive-devel
BuildRequires:  ghc-tagged-devel
BuildRequires:  ghc-transformers-devel
# End cabal-rpm deps

%description
Profunctors.


%package devel
Summary:        Haskell %{pkg_name} library development files
Provides:       %{name}-static = %{version}-%{release}
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development files.


%prep
%setup -q -n %{pkg_name}-%{version}


%build
%ghc_lib_build


%install
%ghc_lib_install


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%license LICENSE


%files devel -f %{name}-devel.files
%doc CHANGELOG.markdown README.markdown


%changelog
* Fri Jul 21 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 5.2-4
- Bump for Fedora 26.

* Sat Dec 17 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.2-3
- Bump to rebuild against new dependencies

* Fri Dec 16 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 5.2-2
- Update release to be newer than previous builds

* Fri Dec 16 2016 Fedora Haskell SIG <haskell@lists.fedoraproject.org> - 5.2-1
- spec file generated by cabal-rpm-0.10.0
