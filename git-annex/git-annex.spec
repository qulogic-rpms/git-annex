# https://fedoraproject.org/wiki/Packaging:Haskell

%bcond_without ikiwiki

Name:           git-annex
Version:        6.20170520
Release:        1%{?dist}
Summary:        Manage files with git, without checking their contents into git

License:        GPLv3+ and AGPLv3+
Url:            https://hackage.haskell.org/package/%{name}
Source0:        https://hackage.haskell.org/package/%{name}-%{version}/%{name}-%{version}.tar.gz
# These are missing from tarball, but will be added back.
Source1:        bash-completion.bash
Source2:        index.mdwn
Patch0:         %{name}-%{version}-flags.patch

BuildRequires:  bash-completion
BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
BuildRequires:  cabal-install
# Begin cabal-rpm deps:
BuildRequires:  ghc-DAV-devel
BuildRequires:  ghc-IfElse-devel
BuildRequires:  ghc-QuickCheck-devel
BuildRequires:  ghc-SafeSemaphore-devel
BuildRequires:  ghc-aeson-devel
BuildRequires:  ghc-async-devel
BuildRequires:  ghc-aws-devel
BuildRequires:  ghc-blaze-builder-devel
BuildRequires:  ghc-bloomfilter-devel
BuildRequires:  ghc-byteable-devel
BuildRequires:  ghc-bytestring-devel
BuildRequires:  ghc-case-insensitive-devel
BuildRequires:  ghc-clientsession-devel
BuildRequires:  ghc-concurrent-output-devel
BuildRequires:  ghc-conduit-devel
BuildRequires:  ghc-conduit-extra-devel
BuildRequires:  ghc-containers-devel
BuildRequires:  ghc-crypto-api-devel
BuildRequires:  ghc-cryptonite-devel
BuildRequires:  ghc-data-default-devel
BuildRequires:  ghc-dbus-devel
BuildRequires:  ghc-directory-devel
BuildRequires:  ghc-disk-free-space-devel
BuildRequires:  ghc-dlist-devel
BuildRequires:  ghc-dns-devel
BuildRequires:  ghc-edit-distance-devel
BuildRequires:  ghc-esqueleto-devel
BuildRequires:  ghc-exceptions-devel
BuildRequires:  ghc-fdo-notify-devel
BuildRequires:  ghc-feed-devel
BuildRequires:  ghc-filepath-devel
BuildRequires:  ghc-free-devel
BuildRequires:  ghc-hinotify-devel
BuildRequires:  ghc-hslogger-devel
BuildRequires:  ghc-http-client-devel
BuildRequires:  ghc-http-conduit-devel
BuildRequires:  ghc-http-types-devel
BuildRequires:  ghc-magic-devel
BuildRequires:  ghc-memory-devel
BuildRequires:  ghc-monad-control-devel
BuildRequires:  ghc-monad-logger-devel
BuildRequires:  ghc-mountpoints-devel
BuildRequires:  ghc-mtl-devel
BuildRequires:  ghc-network-devel
BuildRequires:  ghc-network-info-devel
BuildRequires:  ghc-network-multicast-devel
BuildRequires:  ghc-network-uri-devel
BuildRequires:  ghc-old-locale-devel
BuildRequires:  ghc-optparse-applicative-devel
BuildRequires:  ghc-path-pieces-devel
BuildRequires:  ghc-persistent-devel
BuildRequires:  ghc-persistent-sqlite-devel
BuildRequires:  ghc-persistent-template-devel
BuildRequires:  ghc-process-devel
BuildRequires:  ghc-random-devel
BuildRequires:  ghc-regex-tdfa-devel
BuildRequires:  ghc-resourcet-devel
BuildRequires:  ghc-sandi-devel
BuildRequires:  ghc-securemem-devel
BuildRequires:  ghc-shakespeare-devel
BuildRequires:  ghc-socks-devel
BuildRequires:  ghc-split-devel
BuildRequires:  ghc-stm-chans-devel
BuildRequires:  ghc-stm-devel
BuildRequires:  ghc-template-haskell-devel
BuildRequires:  ghc-text-devel
BuildRequires:  ghc-time-devel
BuildRequires:  ghc-torrent-devel
BuildRequires:  ghc-transformers-devel
BuildRequires:  ghc-unix-compat-devel
BuildRequires:  ghc-unix-devel
BuildRequires:  ghc-unordered-containers-devel
BuildRequires:  ghc-utf8-string-devel
BuildRequires:  ghc-uuid-devel
BuildRequires:  ghc-wai-devel
BuildRequires:  ghc-wai-extra-devel
BuildRequires:  ghc-warp-devel
BuildRequires:  ghc-warp-tls-devel
BuildRequires:  ghc-yesod-core-devel
BuildRequires:  ghc-yesod-default-devel
BuildRequires:  ghc-yesod-devel
BuildRequires:  ghc-yesod-form-devel
BuildRequires:  ghc-yesod-static-devel
# End cabal-rpm deps
BuildRequires:  git
%if %{with ikiwiki}
BuildRequires:  ikiwiki
BuildRequires:  perl(Image::Magick)
%endif

# XXX: feature flag dependencies:
#Benchmark:
#  BuildRequires:  ghc-criterion-devel (unpackaged)
#  BuildRequires:  ghc-deepseq-devel
#TestSuite:
#  BuildRequires:  ghc-crypto-api-devel
#  BuildRequires:  ghc-tasty-devel (unpackaged)
#  BuildRequires:  ghc-tasty-hunit-devel (unpackaged)
#  BuildRequires:  ghc-tasty-quickcheck-devel (unpackaged)
#  BuildRequires:  ghc-tasty-rerun-devel (unpackaged)

Requires:       git

%description
Git-annex allows managing files with git, without checking the file contents
into git. While that may seem paradoxical, it is useful when dealing with files
larger than git can currently easily handle, whether due to limitations in
memory, time, or disk space.

It can store large files in many places, from local hard drives, to a large
number of cloud storage services, including S3, WebDAV, and rsync, with a dozen
cloud storage providers usable via plugins. Files can be stored encrypted with
gpg, so that the cloud storage provider cannot see your data.
git-annex keeps track of where each file is stored, so it knows how many copies
are available, and has many facilities to ensure your data is preserved.

git-annex can also be used to keep a folder in sync between computers, noticing
when files are changed, and automatically committing them to git and
transferring them to other computers. The git-annex webapp makes it easy to set
up and use git-annex this way.

%if %{with ikiwiki}
%package docs
Summary:        %{summary}
Group:          Documentation
BuildArch:      noarch

%description docs
This package contains the documentation for %{name} in HTML format.
%endif

%prep
%setup -q -b 0
cp %SOURCE1 .
cp %SOURCE2 doc/
%patch0 -p1 -b .flags


%build
# XXX: Use cabal configure instead of ./Setup configure.
# https://git-annex.branchable.com/bugs/OOM_while_configuring_git-annex/#comment-70d6184aacd394586831d21c7cfd310b
%global cabal \
LANG=en_US.utf8 \
cabal

%ghc_bin_build

LC_ALL=C TZ=UTC ikiwiki doc html -v --wikiname git-annex \
	--plugin=goodstuff \
	--no-usedirs --disable-plugin=openid --plugin=sidebar \
	--underlaydir=/dev/null --set deterministic=1 \
	--disable-plugin=shortcut --disable-plugin=smiley \
	--plugin=comments --set comments_pagespec="*" \
	--exclude='news/.*' --exclude='design/assistant/blog/*' \
	--exclude='bugs/*' --exclude='todo/*' --exclude='forum/*' \
	--exclude='users/*' --exclude='devblog/*' --exclude='thanks'


%install
%ghc_bin_install

bash_completion_dir=%{buildroot}$(pkg-config --variable=completionsdir bash-completion)
mkdir -p $bash_completion_dir
install -m 644 bash-completion.bash $bash_completion_dir/git-annex


%files
%license COPYRIGHT doc/license/GPL doc/license/AGPL
%doc README CHANGELOG
%attr(755,root,root) %{_bindir}/%{name}
%{_bindir}/%{name}-shell
%{_bindir}/git-remote-tor-annex
%{_mandir}/man1/git-annex*
%{_mandir}/man1/git-remote-tor-annex*
%{_datadir}/bash-completion/

%if %{with ikiwiki}
%files docs
%license doc/license/GPL
%doc html/
%endif


%changelog
* Sat Dec 17 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20161210-1
- update to 6.20161210

* Sat Dec 17 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160613-2
- spec file partially re-generated by cabal-rpm-0.10.0
- Enable "s3" and "webdav" flags

* Sat Jun 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160613-1
- update to 6.20160613

* Sun May 29 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160527-2
- Fix documentation packaging
- Add and install bash-completion

* Sun May 29 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160527-1
- update to 6.20160527

* Fri May 13 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160511-3
- Properly remove S3 and WebDAV deps

* Fri May 13 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160511-2
- Disable S3 and WebDAV support; deps not ready yet

* Fri May 13 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160511-1
- update to 6.20160511
- switch back to flag patching, which doesn't confuse cabal-rpm as much

* Thu May 05 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160419-6
- Enable S3, WebDAV, and Torrent support

* Mon May 02 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160419-5
- Enable XMPP support

* Mon May 02 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160419-4
- Add more BR and enable more flags to enable the assistant

* Sun May 01 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160419-3
- Correctly enable build flags for MagicMime, Pairing, and Webapp

* Sun May 01 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160419-2
- Enable MagicMime, Pairing, and Webapp

* Fri Apr 29 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 6.20160419-1
- Update to 6.20160419

* Sun Aug 23 2015 Ben Boeckel <mathstuf@gmail.com> - 5.20150812-1
- update to 5.20150812

* Sat Aug 08 2015 Ben Boeckel <mathstuf@gmail.com> - 5.20140717-8
- rebuild for ghc-edit-distance and ghc-bloomfilter

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.20140717-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu Apr 09 2015 Richard W.M. Jones <rjones@redhat.com> - 5.20140717-6
- Bump release and rebuild because of ghc-dbus dependency.

* Thu Mar  5 2015 Jens Petersen <petersen@redhat.com> - 5.20140717-5
- rebuild

* Tue Jan 27 2015 Jens Petersen <petersen@redhat.com> - 5.20140717-4
- cblrpm refresh

* Fri Sep  5 2014 Jens Petersen <petersen@redhat.com> - 5.20140717-3
- rebuild

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.20140717-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Jul 23 2014 Ben Boeckel <mathstuf@gmail.com> - 5.20140717-1
- Update to 5.20140717
- disable DesktopNotify until fdo-notify packaged

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 5.20140221-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Mar 26 2014 Ben Boeckel <mathstuf@gmail.com> - 5.20140221-2
- Rebuild for dbus rebuild

* Tue Feb 25 2014 Jens Petersen <petersen@redhat.com> - 5.20140221-1
- update to 5.20140221
- BR cryptohash

* Fri Feb 07 2014 Jens Petersen <petersen@redhat.com> - 5.20140116-1
- update to 5.20140116

* Wed Jan 22 2014 Jens Petersen <petersen@redhat.com> - 5.20140108-2
- rebuild

* Tue Jan 14 2014 Jens Petersen <petersen@redhat.com> - 5.20140108-1
- update to 5.20140108

* Tue Jan 14 2014 Jens Petersen <petersen@redhat.com> - 4.20130827-2
- enable dbus

* Wed Sep 04 2013 Jens Petersen <petersen@redhat.com> - 4.20130827-1
- update to 4.20130827
- update deps and default flags
- build with regex-tdfa

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.20130207-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Jun 12 2013 Jens Petersen <petersen@redhat.com> - 3.20130207-1
- update to 3.20130207
- requires SafeSemaphore

* Thu Jun  6 2013 Jens Petersen <petersen@redhat.com> - 3.20121009-5
- IfElse is now in Fedora

* Fri Mar 22 2013 Jens Petersen <petersen@redhat.com> - 3.20121009-4
- rebuild

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.20121009-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Tue Dec 11 2012 Jens Petersen <petersen@redhat.com> - 3.20121009-2
- create manpages when no ikiwiki

* Tue Nov 06 2012 Jens Petersen <petersen@redhat.com> - 3.20121009-1
- update to 3.20121009

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.20120615-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Jun 26 2012 Jens Petersen <petersen@redhat.com> - 3.20120615-2
- fix doc file symlinks
- BR ImageMagick-perl
- make docs subpackage noarch

* Sat Jun 23 2012 Ben Boeckel <mathstuf@gmail.com> - 3.20120615-1
- Update to 3.20120615

* Tue May 22 2012 Jens Petersen <petersen@redhat.com> - 3.20120522-1
- create git-annex-shell symlink
- build and include manpages and docs in a subpackage

* Sat Apr 21 2012 Ben Boeckel <mathstuf@gmail.com> - 3.20120418-1
- Update to 3.20120418

* Fri Mar 02 2012 Ben Boeckel <mathstuf@gmail.com> - 3.20120229-1
- Update to 3.20120229

* Fri Mar  2 2012 Fedora Haskell SIG <haskell-devel@lists.fedoraproject.org>
- spec file template generated by cabal2spec-0.25.4
